﻿//Минимальная выпуклая 3D оболочка

#include <iostream>
#include <vector>
#include <cmath>
#include <stack>
#include <iomanip>
#include <float.h>

using coordinate = int64_t;
using angle_sz = long double;

struct Point;
coordinate Det2x2(coordinate a11, coordinate a12, coordinate a21, coordinate a22);
Point VectorProduct(const Point& A, const Point& B, const Point& C);
angle_sz GetLenghtVector(Point A, Point B);

struct Point
{
    coordinate x;
    coordinate y;
    coordinate z;
    Point(coordinate x = 0, coordinate y = 0, coordinate z = 0) : x(x), y(y), z(z) {}
    Point operator-(const Point& other) const
    {
        return Point(x - other.x, y - other.y, z - other.z);
    }
    Point operator+(const Point& other) const
    {
        return Point(x + other.x, y + other.y, z + other.z);
    }
    bool operator!= (const Point& other) const
    {
        return (x != other.x || y != other.y || z != other.z);
    }
    bool operator== (const Point& other) const
    {
        return (x == other.x && y == other.y && z == other.z);
    }
};

coordinate Det2x2(coordinate a11, coordinate a12, coordinate a21, coordinate a22)
{
    return a11 * a22 - a12 * a21;
}

coordinate Det3x3(coordinate a11, coordinate a12, coordinate a13, coordinate a21, coordinate a22, coordinate a23, coordinate a31, coordinate a32, coordinate a33)
{
    return a11 * Det2x2(a22, a23, a32, a33) - a12 * Det2x2(a21, a23, a31, a33) + a13 * Det2x2(a21, a22, a31, a32);
}

//[AB, AC]
Point VectorProduct(const Point& A, const Point& B, const Point& C)
{
    Point a = B - A;
    Point b = C - A;
    return Point (Det2x2(a.y, a.z, b.y, b.z), Det2x2(a.x, a.z, b.x, b.z), Det2x2(a.x, a.y, b.x, b.y));
}

//vector AB
angle_sz GetLenghtVector(Point A, Point B = Point(0, 0, 0))
{
    Point vec = B - A;
    angle_sz lenght = std::sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
    return lenght;
}

angle_sz GetAngle2Normal(const Point& n1, const Point& n2)
{
    angle_sz len_n1 = GetLenghtVector(n1);
    angle_sz len_n2 = GetLenghtVector(n2);
    angle_sz scalar_prod = n1.x * n2.x + n1.y * n2.y + n1.z * n2.z;
    if (scalar_prod == 0)
    {
        return 0;
    }
    return std::acos((scalar_prod) / (len_n1 * len_n2));
}

class ConvexHull
{
    struct Flatness
    {
        int first;
        int second;
        int third;
        Point normal; // нормаль к грани, изначально содержащее это ребро
        Flatness(int first, int second, int third, const Point& normal): first(first), second(second), third(third), normal(normal) {}
        int Another(int one, int two)
        {
            if (one == first && two == second || one == second && two == first)
            {
                return third;
            }
            if (one == first && two == third || one == third && two == first)
            {
                return second;
            }
            if (one == third && two == second || one == second && two == third)
            {
                return first;
            }
        }
    };

    struct Edge
    {
        int first;
        int second;
        int flatness; //номер грани 
        bool have_more_2_f = false;
        Edge(int first, int second, int flatness = -1, Point normal = Point(0, 0 , 0)): first(first), second(second), flatness(flatness) {} 
    };

    std::vector<Point> points_;
    std::vector<Flatness> verge_;
    std::vector<Edge> edges_;

    int count_;
    int findMinZ() const;
    angle_sz getAnglePointOXFlatness(const Point& start, const Point& next) const;
    void findFirstFlatness();
    int returnIsEdgeInHull(int a, int b) const;
    void makeHull();
public:
    ConvexHull(const std::vector<Point>& points): points_(points), count_(points.size()) { makeHull();}
    angle_sz FindMinDistance(const Point& base)
    {
        angle_sz min_dist = DBL_MAX;
        for (int i = 0; i < verge_.size(); ++i)
        {
            Point m0 = points_[verge_[i].first];
            Point m1 = points_[verge_[i].second] - m0;
            Point m2 = points_[verge_[i].third] - m0;
            Point b = base - m0;

            angle_sz V = std::abs(Det3x3(m1.x, m1.y, m1.z, m2.x, m2.y, m2.z, b.x, b.y, b.z));
            angle_sz S = GetLenghtVector(VectorProduct(m0, m1 + m0, m2 + m0));

            angle_sz distance = V / S;
            if (distance < min_dist)
            {
                min_dist = distance;
            }
        }
        return min_dist;
    }
};

void ConvexHull::makeHull()
{
    findFirstFlatness();
    std::stack<int> stack;
    stack.push(0);
    stack.push(1);
    stack.push(2);
    Point new_normal;

    while (!stack.empty())
    {
        Edge e = edges_[stack.top()];
        stack.pop();
        if (e.have_more_2_f)
        {
            continue;
        }
        int min_id = -1;
        angle_sz min_angle = 7;

        for (int i = 0; i < count_; ++i)
        {
            int another = verge_[e.flatness].Another(e.first, e.second);
            if (i != another && e.first != i && e.second != i) // не из той же грани
            {
                Point current_normal = VectorProduct(points_[e.second], points_[e.first], points_[i]);
                angle_sz angle = GetAngle2Normal(current_normal, verge_[e.flatness].normal);

                if (min_angle > angle)
                {
                    min_angle = angle;
                    min_id = i;
                    new_normal = current_normal;
                }
            }
        }

        if (min_id != -1)
        {
            e.have_more_2_f = true;
            int count_flatness = verge_.size(); // номер нашей грани
            int first_edge_in_hull = returnIsEdgeInHull(e.first, min_id);
            int second_edge_in_hull = returnIsEdgeInHull(e.second, min_id);

            if (first_edge_in_hull == -1)
            {
                edges_.push_back(Edge(e.first, min_id, count_flatness));
                stack.push(edges_.size() - 1);
            }
            if (second_edge_in_hull == -1)
            {
                edges_.push_back(Edge(min_id, e.second, count_flatness));
                stack.push(edges_.size() - 1);
            }
            if (first_edge_in_hull != -1)
            {
                edges_[first_edge_in_hull].have_more_2_f = true;
            }
            if (second_edge_in_hull != -1)
            {
                edges_[second_edge_in_hull].have_more_2_f = true;
            }

            verge_.push_back(Flatness(e.first, e.second, min_id, new_normal));
        }

    }

}

int ConvexHull::findMinZ() const
{
    int min_id = 0;
    for (int i = 1; i < count_; ++i)
    {
        if (points_[i].z < points_[min_id].z ||
                points_[i].z == points_[min_id].z && points_[i].y < points_[min_id].y ||
                points_[i].z == points_[min_id].z && points_[i].y == points_[min_id].y && points_[i].x < points_[min_id].x)
        {
            min_id = i;
        }
    }
    return min_id;
}

angle_sz ConvexHull::getAnglePointOXFlatness(const Point& start, const Point& next) const
{
    return GetAngle2Normal(start - next, next - Point(next.x, next.y, start.z));
}

void ConvexHull::findFirstFlatness()
{
    int first_point, second_point, third_point;
    first_point = findMinZ();
    angle_sz min_angle = 7;
    int min_id = -1;
    for (int i = 0; i < count_; ++i)
    {
        if (first_point == i)
        {
            continue;
        }

        angle_sz angle = getAnglePointOXFlatness(points_[first_point], points_[i]);
        if (min_angle > angle)
        {
            min_angle = angle;
            min_id = i;
        }
    }
    second_point = min_id;

    min_angle = 7;
    min_id = -1;
    for (int i = 0; i < count_; ++i)
    {
        if (first_point == i || second_point == i)
        {
            continue;
        }
        Point normal = VectorProduct(points_[first_point], points_[second_point], points_[i]);
        angle_sz angle = -1 * normal.z / GetLenghtVector(normal);
        if (min_angle > angle)
        {
            min_angle = angle;
            min_id = i;
        }
    }
    third_point = min_id;

    // правильное ориентирование
    if (VectorProduct(points_[first_point], points_[second_point], points_[third_point]).z > 0)
    {
        std::swap (second_point, third_point);
    }
    Point new_normal = VectorProduct(points_[first_point], points_[second_point], points_[third_point]);
    verge_.emplace_back(Flatness(first_point, second_point, third_point, new_normal)); // перая грань
    edges_.emplace_back(Edge(first_point, second_point, 0));
    edges_.emplace_back(Edge(second_point, third_point, 0));
    edges_.emplace_back(Edge(third_point, first_point, 0));
}

int ConvexHull::returnIsEdgeInHull(int a, int b) const
{
    for (int i = 0; i < edges_.size(); ++i)
    {
        if (edges_[i].first == a && edges_[i].second == b || edges_[i].first == b && edges_[i].second == a)
        {
            return i;
        }
    }

    return -1;
}

int main()
{
    int N;
    Point current;
    std::vector<Point> points;
    std::cin >> N;
    for (int i = 0; i < N; ++i)
    {
        std::cin >> current.x >> current.y >> current.z;
        points.push_back(current);
    }
    ConvexHull ch(points);
    int M;
    std::cin >> M;
    for (int i = 0; i < M; ++i)
    {
        std::cin >> current.x >> current.y >> current.z;
        std::cout << std::setprecision(6) << ch.FindMinDistance(current) << std::endl;
    }
}
