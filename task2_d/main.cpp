﻿// Сумма Минковского

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using coordinate  = long double;
using EPS = 0.000001;

struct Point
{
    coordinate x;
    coordinate y;
    Point(coordinate x = 0, coordinate y = 0): x(x), y(y) {}
    Point operator- (const Point& other) const
    {
        return Point(x - other.x, y - other.y);
    }
    Point operator+ (const Point& other) const
    {
        return Point(x + other.x, y + other.y);
    }
    Point& operator+=(const Point& other)
    {
       x += other.x;
       y += other.y;
       return *this;
    }
    Point& operator-=(const Point& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }
    bool operator==(const Point& other)
    {
        return (x == other.x && y == other.y);
    }
};

class Line
{
    static const int SHIFT = 100;
    Point a_;
    Point b_;
public:
    Line(const Point& a, const Point& b): a_(a), b_(b) {}
    std::pair<double, double> ReturnLine() const
    {
        double k = (a_.y - b_.y) / (a_.x - b_.x);
        double c = b_.y - k * b_.x;
        return std::make_pair(k, c);
    }
};


bool ContainsPoint(const Point& point, std::vector<Point> vertices)
{
    size_t size = vertices.size();
    int count_cross = 0;
    for (size_t i = 0; i < size; ++i)
    {
        size_t i2 = (i + 1) % size;
        if ((point.y <= vertices[i].y && point.y > vertices[i2].y) || (point.y >= vertices[i].y && point.y < vertices[i2].y))
        {
            std::pair<double, double> line = (Line(vertices[i], vertices[i2])).ReturnLine();
            double x = (point.y - line.second) / line.first;
            if (x >= point.x)
                ++count_cross;
        }
        if (vertices[i].y == point.y)
        {
            if ((vertices[i2].y >= point.y && vertices[((i + size) - 1) % size].y < point.y) ||
                (vertices[i2].y < point.y && vertices[((i + size) - 1) % size].y > point.y))
                ++count_cross;
        }
    }
    if (count_cross % 2 == 1)
        return true;
    return false;
}

bool isEqual(double a, double b)
{

    if (a - EPS <= b && a + EPS >= b)
    {
        return true;
    }
    return false;
}

double mod2PI(double angle)
{
    if (angle >= 2 * M_PI || isEqual(angle, 2 * M_PI))
    {
        return mod2PI(angle - 2 * M_PI);
    }
    return angle;
}

class SumMinkovskiy
{
private:
    std::vector<Point> polygon1_, polygon2_;
    std::vector<Point> sum_;
    Point start1_, start2_;
    int pointer1_ = 0, pointer2_ = 0;
    int size1_, size2_;
    int findMinY(const std::vector<Point>& points) const
    {
        int min_id = 0;
        for (int i = 1; i < points.size(); ++i)
        {
            if (points[i].y < points[min_id].y || (points[i].y == points[min_id].y && points[i].x < points[min_id].x))
            {
                min_id = i;
            }
        }
        return min_id;
    }

    void makeRadiusVectors(std::vector<Point>& points, const Point& min)
    {
        for (int i = 0; i < points.size(); ++i)
        {
            points[i] = points[i] - min;
        }
    }

    bool compareByPolarAngle(int index1, int index2)
    {
        Point a = polygon1_[index1];
        Point b = polygon2_[index2];
        a -= polygon1_[(size1_ + index1 - 1) % size1_];
        b -= polygon2_[(size2_ + index2 - 1) % size2_];

        double A = mod2PI(std::atan2(a.y, a.x) + 2 * M_PI);
        double B = mod2PI(std::atan2(b.y, b.x) + 2 * M_PI);
        return A < B;

        if (a.x * b.x >= 0)
        {
            if (a.x < 0 && b.x < 0)
            {
                return std::atan2(a.y, a.x) > std::atan2(b.y, b.x); // 3,4 четверть
            }
            return std::atan2(a.y, a.x) < std::atan2(b.y, b.x);
        }
        else
        {
            if (a.x < 0)
            {
                return false;
            }
            return true;
        }
    }

    void increasePtr(int number_of_polygon)
    {
        if (number_of_polygon == 1)
        {
            pointer1_ = (pointer1_ + 1) % size1_;
        }
        else if (number_of_polygon == 2)
        {
            pointer2_ = (pointer2_ + 1) % size2_;
        }
    }

    int previousPtr(int number_of_polygon)
    {
        if (number_of_polygon == 1)
        {
            return (pointer1_ + size1_ - 1) % size1_;
        }
        else if (number_of_polygon == 2)
        {
            return (pointer2_ + size2_ - 1) % size2_;
        }
    }

    void makeSymmetry(std::vector<Point>& points)
    {
        for (int i = 0; i < points.size(); ++i)
        {
            points[i].x = -(points[i].x);
            points[i].y = -(points[i].y);
        }
    }

    bool isRightRotate(const Point& a_other, const Point& c_other, const Point& b_other)
    {
        Point a = a_other - c_other;
        Point b = b_other - c_other;
        long double angle = a.x * b.y - a.y * b.x;
        return angle >= 0;
    }

    void makeRightVectors(const Point& start)
    {
        sum_[0] += start;
        for (int i = 1; i < sum_.size(); ++i)
        {
            sum_[i] += sum_[i - 1];
        }
    }

    void makeSumMinkovskiy()
    {
        pointer1_ = findMinY(polygon1_);
        pointer2_ = findMinY(polygon2_);
        start1_ = polygon1_[pointer1_];
        start2_ = polygon2_[pointer2_];

        makeRadiusVectors(polygon1_, start1_);
        makeRadiusVectors(polygon2_, start2_);

        int count_elem1 = 0, count_elem2 = 0;
        increasePtr(1);
        increasePtr(2);
        sum_.push_back(Point(0, 0)); //started point

        while (!(count_elem1 == size1_ && count_elem2 == size2_))
        {
            if (count_elem1 < size1_ && count_elem2 < size2_)
            {
                if (compareByPolarAngle(pointer1_, pointer2_))
                {
                    sum_.push_back(polygon1_[pointer1_] - polygon1_[previousPtr(1)]);
                    increasePtr(1);
                    ++count_elem1;
                }
                else
                {
                    sum_.push_back(polygon2_[pointer2_] - polygon2_[previousPtr(2)]);
                    increasePtr(2);
                    ++count_elem2;
                }
            }
            else if (count_elem1 < size1_)
            {
                sum_.push_back(polygon1_[pointer1_] - polygon1_[previousPtr(1)]);
                increasePtr(1);
                ++count_elem1;
            }
            else if (count_elem2 < size2_)
            {
                sum_.push_back(polygon2_[pointer2_] - polygon2_[previousPtr(2)]);
                increasePtr(2);
                ++count_elem2;
            }
        }

        makeRightVectors(start1_ + start2_);
    }
public:
    SumMinkovskiy(const std::vector<Point>& a, const std::vector<Point>& b): polygon1_(a), polygon2_(b)
    {
        std::reverse(std::begin(polygon1_), std::end(polygon1_));
        std::reverse(std::begin(polygon2_), std::end(polygon2_));

        makeSymmetry(polygon2_);
        size1_ = polygon1_.size();
        size2_ = polygon2_.size();
        makeSumMinkovskiy();
    }
    bool HaveIntersection()
    {
        Point base(0,0);
        return ContainsPoint(base, sum_);
    }
};

int main()
{
    Point current;
    int N, M;
    std::vector<Point> polygon1, polygon2;
    std::cin >> N;
    for (int i = 0; i < N; ++i)
    {
        std::cin >> current.x >> current.y;
        polygon1.push_back(current);
    }
    std::cin >> M;
    for (int i = 0; i < M; ++i)
    {
        std::cin >> current.x >> current.y;
        polygon2.push_back(current);
    }

    SumMinkovskiy sm(polygon1, polygon2);
    bool intersection = sm.HaveIntersection();
    if (intersection)
    {
        std::cout << "YES" << std::endl;
    }
    else
    {
        std::cout << "NO" << std::endl;
    }
}