#include  <iostream>
#include <string>
#include <vector>


class SuffixArray
{
private:
	const std::string str_;
	static const unsigned int alphabet = 256;
	std::vector<int> sufarray_;
	std::vector<int> equivalence_;
	const int size_;
	int classes_;

	void findEquivalenceClass()
	{
		classes_ = 1;
		equivalence_[sufarray_[0]] = 0;
		for (int i = 1; i < size_; ++i)
		{
			if (str_[sufarray_[i]] != str_[sufarray_[i - 1]])
				++classes_;
			equivalence_[sufarray_[i]] = classes_ - 1;
		}
	}

	void sortByFirstLetter()
	{
		std::vector<int> count_elem(alphabet, 0);
		for (int i = 0; i < size_; ++i)
			++count_elem[str_[i]];
		for (int i = 1; i < alphabet; ++i)
			count_elem[i] += count_elem[i - 1];
		for (int i = 0; i < size_; ++i)
			sufarray_[--count_elem[str_[i]]] = i;
	}

	void makeSuffixArray()
	{
		sortByFirstLetter();
		findEquivalenceClass();
		std::vector<int> permutation_new(size_, -1);
		std::vector<int> equivalence_new(size_, 0);
		std::vector<int> count(size_);
		for (int h = 0; (1 << h) < size_; ++h) // go throw all 2^h
		{
			for (int i = 0; i < size_; ++i)
			{
				permutation_new[i] = sufarray_[i] - (1 << h);
				if (permutation_new[i] < 0)
					permutation_new[i] += size_;
			}
			for (int i = 0; i < classes_; ++i)
				count[i] = 0;
			for (int i = 0; i < size_; ++i)
				++count[equivalence_[permutation_new[i]]];
			for (int i = 1; i < classes_; ++i)
				count[i] += count[i - 1];
			for (int i = size_ - 1; i >= 0; --i)
				sufarray_[--count[equivalence_[permutation_new[i]]]] = permutation_new[i];
			equivalence_new[sufarray_[0]] = 0;
			classes_ = 1;
			for (int i = 1; i < size_; ++i)
			{
				int middle_1 = (sufarray_[i] + (1 << h)) % size_;
				int middle_2 = (sufarray_[i - 1] + (1 << h)) % size_;
				if (equivalence_[sufarray_[i]] != equivalence_[sufarray_[i - 1]] ||
						equivalence_[middle_1] != equivalence_[middle_2])
					++classes_;
				equivalence_new[sufarray_[i]] = classes_ - 1;
			}
			equivalence_ = equivalence_new;
		}
	}
public:
	SuffixArray(const std::string& str): str_(str), size_(str.size()), 
			sufarray_(std::vector<int>(str.size(), -1)), 
			equivalence_(std::vector<int>(str.size()))
	{
		makeSuffixArray();
	}
	const std::vector<int>& GetSuffixArray()
	{
		return sufarray_;
	}
};

std::vector<int> LCP(const std::string& str, const std::vector<int>& sufarray)
{
	const int size = sufarray.size();
	std::vector<int> lcp(size);
	std::vector<int> reverse_suf(size); //  reverse to the sufarray
	for (int i = 0; i < size; ++i)
		reverse_suf[sufarray[i]] = i;
	int k = 0;
	for (int i = 0; i < size; ++i)
	{
		if (k > 0)
			--k;
		if (reverse_suf[i] == size - 1)
		{
			lcp[size - 1] = -1;
			k = 0;
			continue;
		}
		else
		{
			int j = sufarray[reverse_suf[i] + 1];
			while (std::max(i + k, j + k) < size &&
					str[i + k] == str[j + k])
				++k;
			lcp[reverse_suf[i]] = k;
		}
	}
	lcp.pop_back();
	return lcp;
}

int findDifferentSuf(const std::vector<int>& lcp, const std::vector<int>& sufarray)
{
	int count = 0;
	int size = sufarray.size();
	for (int i = 0; i < size; ++i)
	{
		count += (size - sufarray[i]);
	}
	for (int i = 0; i < lcp.size(); ++i)
	{
		count -= lcp[i];
	}
	return count;
}

int main()
{
	std::string str;
	std::cin >> str;
	SuffixArray suf (str);
	std::vector<int> sufarray = suf.GetSuffixArray();
	auto lcp = LCP(str, sufarray);
	std::cout << findDifferentSuf(lcp, sufarray) << std::endl;
	return 0;
}