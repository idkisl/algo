#include <iostream>
#include <vector>
#include <map>

class SuffixTree
{
public:
	SuffixTree(const std::string& a, const std::string& b) : size_str_a_(a.size()), size_str_b_(b.size()), input_str_(a + b)
	{
		input_size_ = input_str_.size();
		tree_nodes_.resize(input_str_.size());

		tree_size_ = 1;
		for (int i = 0; i < input_size_; ++i)
		{
			makeBigger(i);
		}

	};

	void Print()
	{
		std::cout << tree_size_ << std::endl;

		converByDfs(&tree_nodes_[0]);
		print(&tree_nodes_[0]);
	}

private:
	struct Node
	{
		int parent;
		std::map<char, int> children;

		long long position;
		int left_idx, right_idx;
		int suff_link;
		int string_num;

		Node(int l, int r, int par): left_idx(l), right_idx(r), parent(par), suff_link(-1) {};
		Node(): left_idx(0), right_idx(0), parent(-1), suff_link(-1) {};

		int& getChild(char character)
		{
			return (children.count(character) == 0) ? children[character] = -1 : children[character];
		}

		int getLenghtSubstring()
		{
			return right_idx - left_idx;
		}
	};

	struct State
	{
		int vertex, position;
		State(int v, int pos): vertex(v), position(pos) {}
	};

	std::vector<Node> tree_nodes_;
	State pointer_ = State(0, 0);
	int input_size_;
	int tree_size_;
	size_t size_str_a_;
	size_t size_str_b_;
	std::string input_str_;
	int position_ = 0;

	void makeBigger(int current_pos);
	State checker(State state, int left, int right);
	int split(State current_state);
	int getSuffixLink(int vertex);
	void print(Node* root);
	void converByDfs(Node* root);
};

void SuffixTree::makeBigger(int current_pos)
{
	int mid = true;
	int current = 0;
	while (mid)
	{
		State new_pointer = checker(pointer_, current_pos, current_pos + 1);

		if (new_pointer.vertex != -1)
		{
			pointer_ = new_pointer;
			return;
		}

		mid = split(pointer_);
		current = tree_size_;
		tree_size_++;

		if (tree_size_ >= tree_nodes_.size())
		{
			tree_nodes_.resize(tree_nodes_.size() * 2);
		}

		tree_nodes_[current] = Node(current_pos, input_size_, mid);
		tree_nodes_[mid].getChild(input_str_[current_pos]) = current;

		pointer_.vertex = getSuffixLink(mid);
		pointer_.position = tree_nodes_[pointer_.vertex].getLenghtSubstring();

	}
}

SuffixTree::State SuffixTree::checker(State state, int left, int right)
{
	while (right > left)
	{
		if (state.position == tree_nodes_[state.vertex].getLenghtSubstring())
		{
			state = State(tree_nodes_[state.vertex].getChild(input_str_[left]), 0);

			if (state.vertex == -1)
			{
				return state;
			}

		}
		else
		{

			if (input_str_[tree_nodes_[state.vertex].left_idx + state.position] != input_str_[left])
			{
				return State(-1, -1);
			}
			else if (right - left < tree_nodes_[state.vertex].getLenghtSubstring() - state.position)
			{
				return State(state.vertex, state.position - left + right);
			}

			left += tree_nodes_[state.vertex].getLenghtSubstring() - state.position;
			state.position = tree_nodes_[state.vertex].getLenghtSubstring();
		}
	}

	return state;
}

int SuffixTree::split(State current_state)
{
	if (current_state.position == tree_nodes_[current_state.vertex].getLenghtSubstring())
	{
		return current_state.vertex;
	}
	else if (current_state.position == 0)
	{
		return tree_nodes_[current_state.vertex].parent;
	}
	Node current_vertex = tree_nodes_[current_state.vertex];

	int currrent_idx = tree_size_++;
	if (tree_size_ >= tree_nodes_.size())
	{
		tree_nodes_.resize(tree_nodes_.size() * 2);
	}

	int r_border = current_vertex.left_idx + current_state.position;

	tree_nodes_[currrent_idx] = Node(current_vertex.left_idx, r_border, current_vertex.parent);
	tree_nodes_[current_vertex.parent].getChild(input_str_[current_vertex.left_idx]) = currrent_idx;
	tree_nodes_[currrent_idx].getChild(input_str_[r_border]) = current_state.vertex;
	tree_nodes_[current_state.vertex].parent = currrent_idx;
	tree_nodes_[current_state.vertex].left_idx += current_state.position;

	return currrent_idx;
}

int SuffixTree::getSuffixLink(int vertex)
{
	if (tree_nodes_[vertex].parent == -1)
	{
		return 0;
	}
	else if (tree_nodes_[vertex].suff_link != -1)
	{
		return tree_nodes_[vertex].suff_link;
	}

	int to = getSuffixLink(tree_nodes_[vertex].parent);

	State temp = State(to, tree_nodes_[to].getLenghtSubstring());
	int left = tree_nodes_[vertex].left_idx + (tree_nodes_[vertex].parent == 0);
	int right = tree_nodes_[vertex].right_idx;

	return tree_nodes_[vertex].suff_link = split(checker(temp, left, right));
}

void SuffixTree::print(Node* root)
{
	if (root != &tree_nodes_[0])
	{
		printf("%lld %d %d %d\n",
			tree_nodes_[root->parent].position,
			root->string_num,
			root->left_idx,
			root->right_idx
		);
	}

	for (auto elem : root->children)
	{
		print(&tree_nodes_[elem.second]);
	}

}

void SuffixTree::converByDfs(Node* root)
{
	root->position = position_;

	if (root->left_idx >= size_str_a_)
	{
		root->string_num = 1;
		root->left_idx -= size_str_a_;
		root->right_idx -= size_str_a_;

	}
	else if (root->right_idx >= size_str_a_)
	{
		root->string_num = 0;
		root->right_idx -= size_str_b_;

	}
	else
	{
		root->string_num = 0;
	}

	for (auto elem : root->children)
	{
		position_++;
		converByDfs(&tree_nodes_[elem.second]);
	}
}

int main()
{
	std::string a;
	std::cin >> a;
	std::string b;
	std::cin >> b;

	SuffixTree suf_tree(a, b);
	suf_tree.Print();

	return 0;
}