#pragma warning(disable : 4996)
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <vector>
#include <complex>
#include <fstream>

using std_base = double;
typedef std::complex<std_base> base;

template<class T>
class Furie
{
	std::vector<T> result_;
	void FFT(std::vector<base>& coefficients, bool invert)
	{
		int n = coefficients.size();
		if (n == 1)
		{
			return;
		}
		std::vector<base> A0, A1;
		for (int i = 0; i < n; ++i)
		{
			if (i % 2 == 0)
			{
				A0.push_back(coefficients[i]);
			}
			else
			{
				A1.push_back(coefficients[i]);
			}
		}
		FFT(A0, invert);
		FFT(A1, invert);
		std_base angle = M_PI * 2 / n * (invert ? -1 : 1);
		base w(1), wn(std::cos(angle), std::sin(angle));
		for (int i = 0; i < n / 2; ++i)
		{
			coefficients[i] = A0[i] + w * A1[i];
			coefficients[n / 2 + i] = A0[i] - w * A1[i];
			if (invert)
			{
				coefficients[i] /= 2;
				coefficients[n / 2 + i] /= 2;
			}
			w *= wn;
		}
	}

	int nearUpDegree(int n)
	{
		int d = 1;
		while (d < n)
		{
			d <<=1;
		}
		return d;
	}

	void make2degree(std::vector<base>& vec)
	{
		int n = vec.size();
		int k = nearUpDegree(n);
		vec.resize(k);
	}

	void makeInaccuracy(std::vector<base>& complex, double inaccuracy)
	{
		if (inaccuracy < 0 || inaccuracy > 1)
		{
			return;
		}

		int size = complex.size() * (1 - inaccuracy);
		complex.resize(size);
		make2degree(complex);
	}

public:
	Furie(const std::vector<T>& coefficients, double inaccuracy = 0)
	{
		std::vector<base> complex;
		for (int i = 0; i < coefficients.size(); ++i)
		{
			complex.push_back(coefficients[i]);
		}

		make2degree(complex);
		FFT(complex, false);
		makeInaccuracy(complex, inaccuracy);
		FFT(complex, true);

		for (int i = 0; i < complex.size(); ++i)
		{
			result_.push_back(int(complex[i].real() + 0.5));
		}
	}
	std::vector<T> ReturnResult()
	{
		return result_;
	}
};

// ���������, ����������� ��������� WAV �����. - ����� � ����� �������: https://pastebin.com/cq77Tw6P
struct WAVHEADER
{
	char chunkId[4];
	unsigned int chunkSize;
	char format[4];
	char subchunk1Id[4];
	unsigned int subchunk1Size;
	unsigned short audioFormat;
	unsigned short numChannels;
	unsigned int sampleRate;
	unsigned int byteRate;
	unsigned short blockAlign;
	unsigned short bitsPerSample;
	char subchunk2Id[4];
	unsigned int subchunk2Size;
};

struct WAV
{
	WAV(std::ifstream& in_stream)
	{
		in_stream.read(reinterpret_cast<char*>(&header), sizeof(header));
		data.resize(header.subchunk2Size / sizeof(int16_t));
		in_stream.read(reinterpret_cast<char*>(data.data()), header.subchunk2Size);
	}
	void Write(std::ofstream& out_stream) const
	{
		out_stream.write(reinterpret_cast<const char*>(&header), sizeof(header));
		out_stream.write(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(int16_t));
	}
	WAVHEADER header{};
	std::vector<int16_t> data;
};

int main()
{
	std::ifstream fin("speech.wav");
	if (!fin)
	{
		std::cout << "Failed to open input file\n";
		return 1;
	}
	WAV wav(fin);
	std::vector<int16_t> data = wav.data;
	Furie<int16_t> ft(data, 0.8);
	wav.data = ft.ReturnResult();

	std::ofstream fout("out_speach.wav");

	if (!fout)
	{
		std::cout << "Failed to open or create output file\n";
		return 1;
	}

	wav.Write(fout);
	return 0;
}