﻿#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <stack>

enum _condition_ {WIN, LOSS, NON};

static const int field_size = 8;
static const int field_size_x = field_size;
static const int field_size_y = field_size;

struct Coordinate
{
    int x;
    int y;
    Coordinate(int x = -1, int y = -1) : x(x), y(y) {}
    bool operator==(const Coordinate& other) const noexcept
    {
        return x == other.x && y == other.y;
    }
};

struct HashCoordinate
{
    size_t operator() (const Coordinate& coord) const noexcept
    {
        return coord.x + coord.y * 8;
    }
};

struct State
{
    Coordinate thief;
    Coordinate police;
    bool thief_step;
    State (Coordinate thief, Coordinate police, bool thief_step) : thief(thief), police(police), thief_step(thief_step) {}
    bool operator==(const State& other) const noexcept
    {
        return thief == other.thief && police == other.police && thief_step == other.thief_step;
    }
};

struct HashState
{
    size_t operator() (const State& state) const noexcept
    {
        return ((state.police.x + state.police.y * 8) * 1723) ^ ((state.thief.x + state.thief.y * 8) * 3971) ^(state.thief_step * 517);
    }
};

class Game
{
    struct Node
    {
        _condition_ state;
        int degree;
        int degree_to_win = 0;
        Node(_condition_ state = NON, int degree = -1) : state(state), degree(degree) {}
    };
    
    std::vector<std::vector<int>> field_;
    std::unordered_set<Coordinate, HashCoordinate> barriers_;
    std::unordered_map<State, unsigned int, HashState> states_;
    std::vector<Node> nodes_;
    std::stack<State> leaves_;

    Coordinate start_p;
    Coordinate start_t;

    void readField()
    {
        for (int i = 0; i < field_size; ++i)
        {
            for (int j = 0; j < field_size; ++j)
            {
                if (field_[j][i] == 1)
                {
                    barriers_.insert(Coordinate(i,j));
                }
                if (field_[j][i] == 2)
                {
                    start_t = Coordinate(i, j);
                }
                if (field_[j][i] == 3)
                {
                    start_p = Coordinate(i, j);
                }
            }
        }
    }

    bool isNotBarrier(int x, int y) const
    {
        return barriers_.find(Coordinate(x, y)) == barriers_.end();
    }

    bool isNotOverField(int x, int y) const
    {
        return x >= 0 && x < field_size_x && y >= 0 && y < field_size_y;
    }

    bool goThroughtField(const Coordinate& start, const Coordinate& finish, int iter1, int iter2) const
    {
        int x = start.x;
        int y = start.y;
        while (isNotOverField(x, y))
        {
            x += iter1;
            y += iter2;
            if (!isNotBarrier(x, y))
            {
                return false;
            }
            if (x == finish.x && y == finish.y)
            {
                return true;
            }
        }
    }

    bool returnDiogonalCanKill(const Coordinate& start, const Coordinate& finish) const
    {
        bool flag = !goThroughtField(start, finish, 1, 1) || !goThroughtField(start, finish, -1, 1) || 
            !goThroughtField(start, finish, 1, -1) || !goThroughtField(start, finish, -1, -1);
        return !flag;
    }

    bool canKill(Coordinate police, Coordinate thief) const
    {
        // на одной по x или y
        if (police.y == thief.y)
        {
            bool flag = !goThroughtField(police, thief, 1, 0) || !goThroughtField(police, thief, -1, 0);
            return !flag;
        }
        if (police.x == thief.x)
        {
            bool flag = !goThroughtField(police, thief, 0, 1) || !goThroughtField(police, thief, 0, -1);
            return !flag;
        }
        // диагонали
        return returnDiogonalCanKill(police, thief);
    }

    std::vector<State> fromWhereGot(const State& state) const
    {
        std::vector<State> go;
        Coordinate man(state.thief), stranger(state.police);
        if (state.thief_step)
        {
            std::swap(man, stranger);
        }

        for (int i = man.x - 1; i <= man.x + 1; ++i)
        {
            for (int j = man.y - 1; j <= man.y + 1; ++j)
            {
                if ((i != man.x || j != man.y) && isNotBarrier(i, j) && isNotOverField(i, j) && !(stranger == Coordinate(i, j)))
                {
                    if (state.thief_step == false)
                    {
                        go.emplace_back(State(Coordinate(i, j), stranger, true));
                    }
                    else
                    {
                        go.emplace_back(State(stranger, Coordinate(i, j), false));
                    }
                }
            }
        }
        return go;
    }

    int findOutDegree(const State& state) const
    {
        int count = 0;
        Coordinate man(state.thief), stranger(state.police);
        if (state.thief_step)
        {
            std::swap(man, stranger);
        }

        for (int i = man.x - 1; i <= man.x + 1; ++i)
        {
            for (int j = man.y - 1; j <= man.y + 1; ++j)
            {
                if ((i != man.x || j != man.y) && isNotBarrier(i, j) && isNotOverField(i, j) && !(stranger == Coordinate(i, j)))
                {
                    ++count;
                }
            }
        }
        return count;
    }

    void exploreAllStates()
    {
        for (int t_x = 0; t_x < field_size_x; ++t_x)
        {
            for (int t_y = 0; t_y < field_size_y; ++t_y)
            {
                for (int p_x = 0; p_x < field_size_x; ++p_x)
                {
                    for (int p_y = 0; p_y < field_size_y; ++p_y)
                    {
                        if (!isNotBarrier(t_x, t_y) || !isNotBarrier(p_x, p_y) || (t_x == p_x && t_y == p_y))
                        {
                            continue; // недопустимое состояние
                        }
                        bool can_kill = canKill(Coordinate(p_x, p_y), Coordinate(t_x, t_y));
                        
                        for (int t_step = 0; t_step < 2; ++t_step)
                        {
                            State current(Coordinate(t_x, t_y), Coordinate(p_x, p_y), t_step);

                            int current_degree = findOutDegree(current);
                            if (!can_kill && t_y == (field_size_y - 1) && t_step == 1) 
                            {
                                nodes_.emplace_back(Node(WIN, -2));
                                leaves_.push(current);
                                states_.insert({leaves_.top(), (nodes_.size() - 1)});
                            }
                            else if (can_kill)
                            {
                                nodes_.emplace_back(Node(LOSS, -2));
                                leaves_.push(current);
                                states_.insert({ leaves_.top(), (nodes_.size() - 1) });
                            }
                            else
                            {
                                nodes_.emplace_back(Node(NON, current_degree));
                                states_.insert({ current, (nodes_.size() - 1) });
                            }
                        }
                    }
                }
            }
        }
    }

    void DFS(const State& start)
    {
        auto to_go = fromWhereGot(start); // родительские состояния вершины
        int start_index = states_[start];
        for (int i = 0; i < to_go.size(); ++i)
        {
            int index_to_go = states_[to_go[i]];
            if (nodes_[index_to_go].state != NON) // visited
            {
                continue;
            }

            // значит ход из родителя был у вора
            if (to_go[i].thief_step == true)
            {
                if (nodes_[start_index].state == WIN)
                {
                    nodes_[index_to_go].state = WIN; // вор хочет ходить в WIN
                    leaves_.push(to_go[i]);
                }
                else if (nodes_[start_index].state == LOSS)
                {
                    ++nodes_[index_to_go].degree_to_win;
                    if (nodes_[index_to_go].degree <= nodes_[index_to_go].degree_to_win)
                    {
                        nodes_[index_to_go].state = LOSS;
                        leaves_.push(to_go[i]);
                    }
                }
            }
            else if (to_go[i].thief_step == false) // значит ход из родителя был у терминатора
            {
                if (nodes_[start_index].state == LOSS)
                {
                    nodes_[index_to_go].state = LOSS; // терминатор хочет ходить в LOSS
                    leaves_.push(to_go[i]);
                }
                else if (nodes_[start_index].state == WIN)
                {
                    ++nodes_[index_to_go].degree_to_win;
                    if (nodes_[index_to_go].degree <= nodes_[index_to_go].degree_to_win)
                    {
                        nodes_[index_to_go].state = WIN;
                        leaves_.push(to_go[i]);
                    }
                }
            }
        }
    }

    void primaryDFS()
    {
        while (!leaves_.empty())
        {
            State current = leaves_.top();
            leaves_.pop();
            DFS(current);
        }
    }

public:
    Game(const std::vector<std::vector<int>>& field) : field_(field)
    {
        readField();
        exploreAllStates();
        primaryDFS();
    }
    void Print()
    {
        State start(start_t, start_p, true);
        int index = states_[start];
        if (nodes_[index].state == WIN)
        {
            std::cout << "1" << std::endl;
        }
        else if (nodes_[index].state == LOSS)
        {
            std::cout << "-1" << std::endl;
        }
        else
        {
            std::cout << "-1" << std::endl;
        }
    }
};

int main()
{
    std::vector<std::vector<int>> field(8);
    std::string current;
    for (int i = 0; i < field_size; ++i)
    {
        std::cin >> current;
        for (int j = 0; j < field_size; ++j)
        {
            field[i].push_back(current[j] - '0');
        }
    }
    Game game(field);
    game.Print();
}
