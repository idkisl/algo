﻿#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

int mex(std::vector<int> set)
{
    std::sort(set.begin(), set.end());
    int i = 0;
    if (set[0] > 0)
    {
        return 0;
    }
    int k = 1;
    while (k < set.size())
    {
        if (set[k - 1] < i && i < set[k])
        {
            return i;
        }
        else
        {
            if (set[k - 1] >= i)
            {
                ++i;
            }
            else if (set[k] <= i)
            {
                ++k;
            }
        }
    }
    return set[k - 1] + 1;
}

class Execution
{
    int N;
    std::vector<int> induction;
    std::vector<int> first_step;
    void count(int N)
    {
        for (int i = 2; i <= N; ++i)
        {
            std::vector<std::pair<int, int>> cases;
            cases.push_back(std::make_pair(0, i - 1));
            cases.push_back(std::make_pair(1, i - 2));
            for (int j = 2; j < i - 2; ++j)
            {
                cases.push_back(std::make_pair(j, i - j - 1));
            }
            cases.push_back(std::make_pair(i - 1, 0));
            cases.push_back(std::make_pair(i - 2, 1));

            std::vector<int> id;
            for (int j = 0; j < cases.size(); ++j)
            {
                int xor_nim = (induction[cases[j].first] ^ induction[cases[j].second]);
                id.push_back(xor_nim);
                if (i == N && xor_nim == 0)
                {
                    first_step.push_back(cases[j].first + 1);
                }
            }
            int summ = mex(id);

            induction.push_back(summ);
        }
    }
public:
    Execution(int N): N(N)
    {
        induction.push_back(0); // N = 0
        induction.push_back(0); // N = 1
        count(N);
    }
    void PrintResult()
    {
        if (induction.size() <= N)
        {
            std::cout << "ERROR SIZE N";
            return;
        }
        bool winner = induction[N] != 0;
        std::cout << (winner ? "Schtirlitz\n" : "Mueller\n");
        if (winner)
        {
            std::sort(first_step.begin(), first_step.end());
            auto last = std::unique(first_step.begin(), first_step.end());
            first_step.erase(last, first_step.end());

            for (int i = 0; i < first_step.size(); ++i)
            {
                std::cout << first_step[i] << std::endl;
            }
        }
    }
};

int main()
{
    int N;
    std::cin >> N;
    Execution ex(N);
    ex.PrintResult();
}